package org.example.service2.cxf;

/**
 * Created by KarevPA on 09.10.2017.
 */

import org.apache.camel.component.cxf.CxfEndpointConfigurer;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.frontend.AbstractWSDLBasedEndpointFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

public class CustomCxfEndpointConfigurer implements CxfEndpointConfigurer
{
    private String receiveTimeout;
    private String connectionTimeout;

    @Override
    public void configure(AbstractWSDLBasedEndpointFactory abstractWSDLBasedEndpointFactory)
    {

    }

    @Override
    public void configureClient(Client client)
    {
        HTTPConduit conduit = (HTTPConduit) client.getConduit();
        HTTPClientPolicy policy = new HTTPClientPolicy();
        policy.setReceiveTimeout(Long.valueOf(receiveTimeout));
        policy.setConnectionTimeout(Long.valueOf(connectionTimeout));
        conduit.setClient(policy);
    }

    @Override
    public void configureServer(Server server)
    {

    }

    public String getReceiveTimeout() {
        return receiveTimeout;
    }

    public void setReceiveTimeout(String receiveTimeout) {
        this.receiveTimeout = receiveTimeout;
    }

    public void setConnectionTimeout(String connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public String getConnectionTimeout() {
        return connectionTimeout;
    }
}