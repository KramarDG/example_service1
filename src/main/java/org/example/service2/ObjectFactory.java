
package org.example.service2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.example.service2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckBlock_QNAME = new QName("http://service2.example.org/", "checkBlock");
    private final static QName _CheckBlockResponse_QNAME = new QName("http://service2.example.org/", "checkBlockResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.example.service2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckBlock }
     * 
     */
    public CheckBlock createCheckBlock() {
        return new CheckBlock();
    }

    /**
     * Create an instance of {@link CheckBlockResponse }
     * 
     */
    public CheckBlockResponse createCheckBlockResponse() {
        return new CheckBlockResponse();
    }

    /**
     * Create an instance of {@link Request }
     * 
     */
    public Request createRequest() {
        return new Request();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckBlock }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CheckBlock }{@code >}
     */
    @XmlElementDecl(namespace = "http://service2.example.org/", name = "checkBlock")
    public JAXBElement<CheckBlock> createCheckBlock(CheckBlock value) {
        return new JAXBElement<CheckBlock>(_CheckBlock_QNAME, CheckBlock.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckBlockResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CheckBlockResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service2.example.org/", name = "checkBlockResponse")
    public JAXBElement<CheckBlockResponse> createCheckBlockResponse(CheckBlockResponse value) {
        return new JAXBElement<CheckBlockResponse>(_CheckBlockResponse_QNAME, CheckBlockResponse.class, null, value);
    }

}
