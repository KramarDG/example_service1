package com.itprofix.cxf.example;



import org.apache.camel.Exchange;
import org.example.service2.CheckBlock;
import org.example.service2.CheckBlockResponse;
import org.example.service2.Request;

public class Operation {
    public void checkType (StartRegistration registration, Exchange exchange){
        if (registration.getArg0().getPerson() != null){
            if (registration.getArg0().getPerson().getName() != null){
                exchange.getIn().setHeader("NeedReqByPerson",registration.getArg0().getPerson().getName());
            }
        }

        if (registration.getArg0().getOrgfanization() != null){
            if (registration.getArg0().getOrgfanization().getName() != null){
                exchange.getIn().setHeader("NeedReqByOrganization",registration.getArg0().getOrgfanization().getName());
            }
        }

        if (registration.getArg0().getDocument() != null){
            if (registration.getArg0().getDocument().getName() != null){
                exchange.getIn().setHeader("NeedReqByDocument",registration.getArg0().getDocument().getName());
            }
        }
    }

    public CheckBlock prepareReqService2(Exchange exchange){
        CheckBlock out = new CheckBlock();
        Request request = new Request();
        request.setId(exchange.getIn().getHeader("breadcrumbId").toString());
        request.setName(exchange.getIn().getHeader("ReqValue").toString());
        out.setArg0(request);
        return out;
    }

    public StartRegistrationResponse prepareResponse(Exchange exchange){
        StartRegistrationResponse out = new StartRegistrationResponse();
        Registration reg = new Registration();
        reg.setId(exchange.getIn().getHeader("breadcrumbId").toString());
        out.setReturn(reg);
        return out;
    }

    public void setPersonResponse(CheckBlockResponse checkBlockResponse,Exchange exchange){
        exchange.getIn().setHeader("PersonResponse",checkBlockResponse.getReturn().getResultCheck());
    }

    public void setOrgResponse(CheckBlockResponse checkBlockResponse,Exchange exchange){
        exchange.getIn().setHeader("OrgResponse",checkBlockResponse.getReturn().getResultCheck());
    }
}

