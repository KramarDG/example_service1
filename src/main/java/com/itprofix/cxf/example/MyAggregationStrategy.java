package com.itprofix.cxf.example;

import org.apache.camel.Exchange;
import org.apache.camel.PropertyInject;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.HashSet;
import java.util.Map;

/**
 * Created by dmitrenkona on 21.01.2019.
 */
public final class MyAggregationStrategy implements AggregationStrategy {

    private HashSet<String> headersSet = new HashSet<>();



    public MyAggregationStrategy(PropertiesComponent propertiesComponent) throws Exception {
        String headers = propertiesComponent.parseUri("{{headers.for.result}}");
        for (String header : headers.split(",")) {
            headersSet.add(header);
        }
    }

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        if (oldExchange == null) {
            return newExchange;
        } else {
            for (Map.Entry<String, Object> header : oldExchange.getIn().getHeaders().entrySet()) {
                if (headersSet.contains(header.getKey())) {
                    newExchange.getIn().setHeader(header.getKey(), header.getValue());
                }
            }
            return newExchange;
        }
    }
}
