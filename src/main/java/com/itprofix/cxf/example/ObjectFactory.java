
package com.itprofix.cxf.example;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.itprofix.cxf.example package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StartRegistration_QNAME = new QName("http://example.cxf.itprofix.com/", "startRegistration");
    private final static QName _StartRegistrationResponse_QNAME = new QName("http://example.cxf.itprofix.com/", "startRegistrationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.itprofix.cxf.example
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StartRegistration }
     * 
     */
    public StartRegistration createStartRegistration() {
        return new StartRegistration();
    }

    /**
     * Create an instance of {@link StartRegistrationResponse }
     * 
     */
    public StartRegistrationResponse createStartRegistrationResponse() {
        return new StartRegistrationResponse();
    }

    /**
     * Create an instance of {@link Request }
     * 
     */
    public Request createRequest() {
        return new Request();
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link Organization }
     * 
     */
    public Organization createOrganization() {
        return new Organization();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link Registration }
     * 
     */
    public Registration createRegistration() {
        return new Registration();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartRegistration }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link StartRegistration }{@code >}
     */
    @XmlElementDecl(namespace = "http://example.cxf.itprofix.com/", name = "startRegistration")
    public JAXBElement<StartRegistration> createStartRegistration(StartRegistration value) {
        return new JAXBElement<StartRegistration>(_StartRegistration_QNAME, StartRegistration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartRegistrationResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link StartRegistrationResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://example.cxf.itprofix.com/", name = "startRegistrationResponse")
    public JAXBElement<StartRegistrationResponse> createStartRegistrationResponse(StartRegistrationResponse value) {
        return new JAXBElement<StartRegistrationResponse>(_StartRegistrationResponse_QNAME, StartRegistrationResponse.class, null, value);
    }

}
